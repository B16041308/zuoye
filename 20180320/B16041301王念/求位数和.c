#include<stdio.h>
void f1(int n);
void f2(int n);
void f3(int n);
void f4(int n);
void main()
{
	int n;
	scanf("%d",&n);
	f1(n);
	f2(n);
	f3(n);
	f4(n);
}
void f1(int n)
{
	int i,x,sum=0;
	for(i=1;i<=n;i++)
	{
    	if(i<10)
    	{
	    	sum=sum+i;
    	}
	    if(i>=10)
	    {
	    	x=i;
	    	for(;x>0;x=x/10)
    		{
	    		sum=sum+x%10; 
	    	}
	    }
	}
	printf("%d\n",sum);
}
void f2(int n)
{
	int i,x,sum=0;
	for(i=1;i<=n;i++)
	{
    	if(i<10)
    	{
    		sum+=i;
    	}
    	if(i>=10)
    	{
    		x=i;
    		do
    		{
    			sum=sum+x%10;
    			x=x/10;
    		}while(x>0); 
    	}
    } 
	printf("%d\n",sum);
}
void f3(int n)
{
	int i,x,sum=0;
    for(i=1;i<=n;i++)
    {
    	if(i<10)
	    {
		    sum+=i;
	    }
	    if(i>=10)
	    {
		    x=i;
		    while(x>0)
		    {
			    sum=sum+x%10;
			    x=x/10;
		    }
	    }
	}
	printf("%d\n",sum);
}
void f4(int n)
{
	int i,x,sum=0;
	for(i=1;i<=n;i++)
	{
        if(i<10)
    	{
	    	sum+=i;
	    }
	    if(i>=10)
	    {
		    x=i;
		    while(1)
		    {
			    sum=sum+x%10;
			    x=x/10;
			    if(x<=0)
			        break;
		    }
	    }
	}
	printf("%d\n",sum);
}
