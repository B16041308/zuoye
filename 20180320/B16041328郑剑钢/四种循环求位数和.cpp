#include<iostream>
using namespace std;
int one(int x)
{
	int count=0;
   while(x)
   {
     count+=x%10;
     x/=10;
   }
   return count;
}
int two(int x)
{
	int count=0;
    do{
	  count+=x%10;
	  x/=10;
	}while(x);
	return count;
}
int three(int x)
{
    int count=0;
	for(int num=x;num;num/=10)
	   count+=num%10;
	return count;
}
int four(int x)
{
    int count=0;
	while(1)
	{
	count+=x%10;
	x/=10;
	  if(x==0)
         break;
	}
	return count;
}
int main()
{
	int x;
	cin>>x;
	cout<<one(x)<<endl;
	cout<<two(x)<<endl;
	cout<<three(x)<<endl;
	cout<<four(x)<<endl;
  return 0;
}
