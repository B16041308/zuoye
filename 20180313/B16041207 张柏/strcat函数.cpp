//用c 语言实现strcat函数
//输入两行，第一行输入一个字符串，第二行另一个字符串
//输出一行，两个字符串连接以后的值
#include<stdio.h>
#include<stdlib.h>
char* strcat(char *str1,char *str2)
{
 char *str3;
 int i=0;
 int j=0;
 int k;
    int count1=0,count2=0;
 while(str1[i]!='\0')
 {
  count1++;
  i++;
 }
 while(str2[j]!='\0')
 {
  count2++;
  j++;
 }
   str3=(char*)malloc(sizeof(char)*(count1+count2+1));
   for(k=0;k<count1;k++)
   {
    str3[k]=str1[k];
   }
   for(k=0;k<count2;k++)
   {
    str3[count1+k]=str2[k];
   }
   str3[count1+count2]='\0';
   return str3;
}
int main()
{
 int i;
 char str1[10]="hello";
 char str2[10]="world";
    printf("%s",strcat(str1,str2));
 printf("\n");
 return 0;
}
