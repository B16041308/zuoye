#strcpy

test.c 为函数测试程序源码,直接运行结果如图所示:

![image](http://homura.cc/CGfiles/res.png)

也可以直接引用 str.h头文件进行测试

```
#include<stdio.h>
#include"str.h"
int main(){
	char * s=strcpy("The string you want to copy");
	printf("%s\n", s);
	return 0;
}
```
//代码中的中文都是utf-8编码，windows下某些编辑器可能会乱码。。。