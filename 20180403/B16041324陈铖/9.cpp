#include<stdio.h>
#include<string.h>

char MoveLeft(char *r){
	char t=r;
    while(1){
       if(t=='\n')
            break;
        t+=2;
        if(t==123)//y->a
       	t='a';
        else if(t==124)//z->b
            t='b';
        else if(t==89)//Y->A
            t='A';
        else if(t==90)//Z->B
            t='B';
    }
    return t;
}

char AntiSort(char *r){
	int i=0;
	int j=strlen(r);
 	char flag;
 	while(i<j){
 		flag=r[i];
 		r[i]=r[j];
 		r[j]=flag;
 		i++;
 		j--;
 	}
}

char WordChange(char *r){
 	char t=r;
 	while(1){
         if(t=='\n')
             break;
         t+=32;
 	}
 	return t;
}

int main(){
 	char *r;
 	getchar(r);
 	MoveLeft(*r);
 	AntiSort(*r);
 	WordChange(*r);
 	putchar(r);
 	return 0;
}
